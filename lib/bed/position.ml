
type t = {
  position: int;
  chrom: string
}

type aligned =  Pos of t | Gap
type pair = (aligned * aligned)

let compare e1 e2 = 
  let pos = compare e1.position e2.position in
  if pos = 0 then compare e1.chrom e2.chrom
  else pos

let distance e1 e2 = abs (e1.position - e2.position)

let of_tuple (chrom, pos) = {position=pos;chrom=chrom}

let partition position_list = 
  (* Partition position list by chromosome 
     Result is a hashtbl of (chrom;position_list)
     Lists are unsorted. *)
  List.fold_left (fun acc pos -> 
      let chrom = pos.chrom in 
      match Hashtbl.find_opt acc chrom with 
      | Some pos_list -> Hashtbl.replace acc chrom (pos::pos_list); acc
      | None -> Hashtbl.add acc chrom [pos]; acc
    ) (Hashtbl.create 0) position_list

let print elt = 
  print_string elt.chrom; 
  print_char '@'; 
  print_int elt.position;
;;

let print_pair (e1, e2) = begin
  begin match e1 with 
    |Pos p1 -> print p1;
    |Gap -> print_string " - ";
  end;
  print_string " :: ";
  begin match e2 with 
    |Pos p2 -> print p2;
    |Gap -> print_string " - ";
  end;
  print_newline();
end
