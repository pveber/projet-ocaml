type t = {
  position: int;
  chrom: string
}

type aligned =  Pos of t | Gap

type pair = (aligned * aligned)

val compare :
  t -> t -> 
  int

val distance :
  t -> t -> 
  int

val of_tuple :
  (string * int) -> 
  t


val partition:
  t list -> 
  (string, t list) Hashtbl.t

val print :
  t -> unit

val print_pair :
  pair -> unit

