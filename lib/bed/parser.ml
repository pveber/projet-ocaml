let parse_line line =
  (* Parse line as (chrom, pos) *)
  String.trim line
  |> String.split_on_char '\t'
  |> (fun line -> match line with
      |(chrom :: _ :: _ :: _ :: summit :: _) -> (chrom, int_of_string summit)
      | _ -> assert false;)

let parse_position line =
  (* Parse line as Position *)
  parse_line line
  |> Position.of_tuple

let parse_bed ~file ~line_parser =
  (* Load file as list of lines *)
  Stdio.In_channel.read_lines file
  (* Filter out empty and commentted lines *)
  |> List.filter (fun line -> String.length line > 0 && line.[0] != '#')
  (* Remove column header line *)
  |> Base.(Fn.flip List.drop 1)
  (* Parse tab separated lines *)
  |> List.map line_parser


let as_dist_matrix pos_list_1 pos_list_2 =
  let matrix = Array.make_matrix
      (List.length pos_list_1)
      (List.length pos_list_2)
      0 in
  List.iteri (fun i p1 ->
      List.iteri (fun j p2 ->
          matrix.(i).(j) <- Position.distance p1 p2
        ) pos_list_2
    ) pos_list_1;
  matrix
