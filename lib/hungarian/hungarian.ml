(* ========== MODULES ======================================================= *)

module Vertex = struct
  type partition = |A |B 
  type t = {
    mutable weight:int; 
    id:int; 
    partition: partition
  }

  let partition_to_char = function
    |A -> 'A'
    |B -> 'B'

  let print vertex = 
    print_int vertex.id; print_char '.';
    print_char (partition_to_char vertex.partition);
    print_string ":"; print_int vertex.weight

  let compare v1 v2 = 
    let part_cmp = compare v1.partition v2.partition in
    if part_cmp = 0 
    then compare v1.id v2.id
    else part_cmp

end (* Module Vertex *)


module VertexSet = struct 
  include Set.Make (Vertex)

  let of_range start n partition = 
    List.fold_left (fun acc id -> 
        add {
          Vertex.weight=0; 
          id=id;
          partition=partition} acc
      ) empty (Core.List.range start n)

  let update_potentials operator vertices delta = 
    iter(fun vertex -> 
        Vertex.(vertex.weight <- operator vertex.weight delta)
      ) vertices

  let _print vertex_set = iter (
      fun v -> Vertex.print v; print_string " ; "
    ) vertex_set

end (* Module VertexSet *)

module Edge = struct
  type t = {
    i:Vertex.t; 
    j:Vertex.t; 
    orientation:bool; 
    weight:int; 
    id:int
  }

  let print edge = 
    let link = if edge.orientation 
      then " <--- "
      else " ---> " in
    print_int edge.id; print_string " : " ; 
    Vertex.print edge.i; print_string link; Vertex.print edge.j;
    print_string " :: "; print_int edge.weight;print_newline();;

  let compare e1 e2 = compare e1.id e2.id

  let delta edge = edge.weight - Vertex.(edge.i.weight + edge.j.weight)

  let is_tight edge = 
    Vertex.(edge.i.weight + edge.j.weight) = edge.weight


  let reversed edge = {edge with orientation = not edge.orientation}


  let are_path edge1 edge2=(((edge1.j = edge2.j) && edge2.orientation) || 
                            ((edge1.i = edge2.i) && edge1.orientation)) && 
                           edge1.orientation != edge2.orientation

end (* Module Edge *)

module EdgeSet  = struct
  include Set.Make (Edge)

  let _print = iter Edge.print

  let vertices edge_set = fold (fun {Edge.i ; j ; _} acc -> 
      acc
      |> VertexSet.add i
      |> VertexSet.add j
    ) edge_set VertexSet.empty


end (* Module EdgeSet *)


module TightGraph = struct 

  type output = {
    weight: int;
    edges: (int*int) list
  }

  type t = {
    sources: VertexSet.t;
    targets: VertexSet.t;
    vertices: VertexSet.t;
    mutable edges: EdgeSet.t;
    edge_pool: EdgeSet.t;
    matching: EdgeSet.t;
  }

  let as_list tight_graph = 
    EdgeSet.fold (
      fun edge matching -> {
          weight = matching.weight + edge.weight;
          edges = (edge.i.id, edge.j.id)::matching.edges}
    ) tight_graph.matching {weight=0;edges=[]}
  (* |> List.sort (fun (a, _) (b, _) -> compare a b)  *)

  let init_vertices matrix = 
    let n = Array.length(matrix) in
    let m = Array.length(matrix.(0)) in
    let v_left = VertexSet.of_range 0 n Vertex.A in
    let v_right = VertexSet.of_range 0 m Vertex.B in
    (v_left, v_right)


  let init_edges v_left v_right weights=
    VertexSet.fold (fun v1 acc -> 
        VertexSet.fold (fun v2 acc -> 
            EdgeSet.add ({Edge.i=v1; 
                          j=v2; 
                          orientation=false; 
                          weight=weights.(v1.Vertex.id).(v2.Vertex.id);
                          id=v1.Vertex.id*(VertexSet.cardinal v_right)+v2.Vertex.id
                         }) acc
          ) v_right acc
      ) v_left EdgeSet.empty


  let init_potentials v_right weights =
    let transWeights = Core.Array.transpose_exn weights in
    VertexSet.iter (fun vertex -> 
        let min_weight = 
          Core.Array.min_elt transWeights.(vertex.Vertex.id) ~compare:Core.Int.compare in
        match min_weight with
          Some min_weight -> vertex.Vertex.weight <- min_weight
        | None -> raise (Invalid_argument "No minimum found in adjacents ??")
      ) v_right

  let init_tight_edges = EdgeSet.filter Edge.is_tight

  let normalize matrix =
    let min_weights = Array.map 
        (fun row -> 
           let m = Core.Array.min_elt row ~compare in 
           match m with
             Some m -> m
           | None -> raise (Invalid_argument "No minimum found in row")
        ) matrix in
    let min_weight = Core.Array.min_elt min_weights ~compare in
    match min_weight with 
      Some min_weight ->
      if min_weight < 0 then
        Array.map (fun row -> 
            Array.map (fun elt -> elt - min_weight) row
          ) matrix
      else matrix
    |None -> raise (Invalid_argument "No minimum found in matrix")
  

  let of_weight_matrix matrix = 
    let normalized = normalize matrix in
    let (v_left, v_right) = init_vertices normalized in
    init_potentials v_right matrix;
    let edges = init_edges v_left v_right normalized in
    {
      sources = v_left;
      targets = v_right;
      vertices = VertexSet.union v_left v_right;
      edges = (init_tight_edges edges);
      edge_pool = edges;
      matching = EdgeSet.empty
    }

  (* ============= Pathfinding ============================================= *)

  let branching edge_set start_edge seen =
    EdgeSet.filter (fun edge -> 
        Edge.are_path start_edge edge && 
        not (VertexSet.mem edge.i seen && VertexSet.mem edge.j seen)
      ) edge_set


  let rec dfs edge_set start_edge seen = 
    let seen = seen
               |> VertexSet.add start_edge.Edge.i
               |> VertexSet.add start_edge.Edge.j
    in 
    let branch = branching edge_set start_edge seen  in
    EdgeSet.fold (
      fun edge acc -> dfs (EdgeSet.remove start_edge edge_set) edge acc
    ) branch seen 

  let rec pathfinder targets start_edge edge_set path= 
    let edge_set = (EdgeSet.remove start_edge edge_set) in
    if VertexSet.mem start_edge.j targets && not start_edge.orientation 
    then Some (start_edge::path)
    else 
      let branches = EdgeSet.filter (
          fun edge -> Edge.are_path start_edge edge
        ) edge_set in
      EdgeSet.fold (fun edge acc -> match acc with 
          |None -> pathfinder targets edge edge_set (start_edge::path)  
          |Some acc -> Some acc) branches None

  let path_from_to sources targets edge_set =
    let start_edges = EdgeSet.filter (fun edge -> 
        VertexSet.mem edge.i sources && not edge.orientation
      ) edge_set in 
    let path = EdgeSet.fold (fun start acc -> match acc with 
        |None -> pathfinder targets start edge_set []
        |Some acc -> Some acc) start_edges None in
    match path with 
    |Some path -> path
    |None -> invalid_arg "No valid path found"

  let reachable_from vertices tight_graph =
    let start_edges = EdgeSet.filter (fun edge -> 
        not edge.orientation && VertexSet.mem edge.i vertices 
      ) tight_graph.edges in
    EdgeSet.fold (fun start_edge acc  ->
        if (VertexSet.mem start_edge.Edge.i vertices && 
            not start_edge.Edge.orientation)
        then dfs tight_graph.edges start_edge acc
        else acc)  start_edges VertexSet.empty


  (* ============= Matching ================================================ *)

  let matching tight_graph = 
    EdgeSet.filter (fun edge -> edge.Edge.orientation) tight_graph.edges

  let is_perfect_matching tight_graph=
    let matched_vertices = EdgeSet.vertices tight_graph.matching in
    VertexSet.equal matched_vertices tight_graph.vertices

  (* let matching_weight matching_edges =
     EdgeSet.fold (fun edge acc -> acc + edge.weight) matching_edges 0 *)

  (* ============= Hungarian blood magic =================================== *)

  let compute_r tight_graph  = 
    let open Edge in
    EdgeSet.fold (
      fun edge (rs,rt) -> if edge.orientation  
        then (VertexSet.remove edge.i rs, VertexSet.remove edge.Edge.j rt)
        else (rs,rt)
    ) tight_graph.edges (tight_graph.sources, tight_graph.targets) 


  let is_linking_sets (edge: Edge.t) left_set right_set = 
    (VertexSet.mem edge.Edge.i left_set) && (VertexSet.mem edge.Edge.j right_set)


  let compute_deltas z_inter_s t_bar_z edges = 
    EdgeSet.fold (fun (edge: Edge.t) acc -> 
        if  is_linking_sets edge z_inter_s t_bar_z 
        then (Edge.delta edge)::acc
        else acc )
      edges [] 


  let reverse_edges_in_path tight_graph path = 
    EdgeSet.map (fun edge -> if List.mem edge path
                  then (Edge.reversed edge)
                  else edge) tight_graph.edges


  let update_tight_edges tight_graph= 
    EdgeSet.union tight_graph.edges (EdgeSet.filter Edge.is_tight tight_graph.edge_pool)


  let rec hungarian_iter tight_graph =
    let tight_graph = {tight_graph with matching = matching tight_graph} in
    if is_perfect_matching tight_graph
    then tight_graph
    else
      let (r_s, r_t) =  compute_r tight_graph in
      let z = VertexSet.union r_s (reachable_from r_s tight_graph) in
      (* Z empty ? switch deltas : invert path  *)
      if VertexSet.is_empty (VertexSet.inter r_t z) 

      then
        let (z_inter_s, t_bar_z, z_inter_t) = (
          VertexSet.inter z tight_graph.sources, 
          VertexSet.diff tight_graph.targets z, 
          VertexSet.inter z tight_graph.targets
        ) in
        let deltas = compute_deltas z_inter_s t_bar_z tight_graph.edge_pool in
        let min_delta = Core.List.min_elt deltas ~compare:Core.Int.compare in 
        begin match min_delta with 
          | Some min_delta ->
            assert (min_delta <> 0);
            VertexSet.update_potentials (+) z_inter_s min_delta;
            VertexSet.update_potentials (-) z_inter_t min_delta;

            (* Update tight graph with potential new tight edges *)
            hungarian_iter {
              tight_graph with 
              edges = update_tight_edges tight_graph
            }
          |None -> raise (Invalid_argument "No minimum delta was found")
        end

      else 
        let inverting_path = path_from_to r_s r_t tight_graph.edges in
        (* Update tight graph by reversing a path from Rs to Rt *)
        hungarian_iter {
          tight_graph with 
          edges = reverse_edges_in_path tight_graph inverting_path
        }



end (* module TightGraph *)


let hungarian weights =
  TightGraph.of_weight_matrix weights
  |> TightGraph.hungarian_iter
  |> TightGraph.as_list