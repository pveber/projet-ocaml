(* Générateur de permutations *)
let rec insert element vector = 
  match vector with 
  | [] -> [[element]]
  | hd::tail -> 
    let l = insert element tail in (* insérer dans la liste plus courte *)
    let insertions = List.map (fun l -> hd::l) l in (* add head to all elements in list  of list *)
    (element::hd::tail) :: insertions;; (* ajouter l'insertion en position 0 *)

let rec permutations vector = 
  match vector with
  | [] -> [[]]
  | hd::tail -> 
    let l = permutations tail in
    let perm = List.map (insert hd) l in
    List.concat perm;;

let score perm adj = 
  let weights = List.mapi (
      fun row column -> adj.(row).(column)
    ) perm in 
  List.fold_left (+) 0 weights;;

let scores permutations adjacency = 
  List.map (fun perm -> 
      score perm adjacency
    ) permutations;;

(* Sélectionne les meilleures permutations*)
let best_perms adj = 
  let perms = permutations (Core.List.range 0 (Array.length adj)) in
  (* Calcule la liste des scores *)
  let scores = scores perms adj in
  (* parcourir score et permutation  en stockant (min_score, bests)*)
  (* si score > min_score, on reset bests *)
  List.fold_left2 (fun (min_score, bests) perm score ->
      if score = min_score then
        (min_score, perm::bests)
      else if score < min_score then
        (score, [perm])
      else 
        (min_score, bests)) (10000, []) perms scores;;

(* Calcule le score sur une liste de permutations
   en se basant sur une matrice d'adjacence *)
(* let adjacency = Array.make_matrix 3 3 1;;
   adjacency.(0).(0) <- 8;;
   adjacency;;

   scores (permutations [0;1;2]) adjacency;;
   best_perms (permutations [0;1;2]) adjacency;; *)
