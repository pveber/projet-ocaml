open Assignment_bed

module Align = struct
  type trace = Ins | Del | Match
  type score = {
    value: int;
    trace: trace;
  }

  let max_elt ~compare l =
    match Core.List.max_elt l ~compare with
    | None -> assert false
    | Some s -> s

  let find_offset p1 p2 i j max_dist =
    match Core.List.find (Core.List.range 1 i ) ~f:(
        fun off -> (Position.distance p1.(i-off) p2.(j)) <= max_dist
      ) with
    | Some offset -> offset
    | None -> max_elt [i;1] ~compare:compare

  let open_score p1 p2 max_dist score_rec x y =
    if x = 0 then
      {value=0;trace=Del}
    else if y = 0 then
      {value=0;trace=Ins}
    else
      let i,j = (x-1, y-1) in
      if Position.distance p1.(i) p2.(j) > max_dist then
        if Position.(p1.(i).position > p2.(j).position) then
          let offset = find_offset p1 p2 i j max_dist in
          {value=(score_rec (x-offset) y).value;
           trace=Ins}
        else
          let offset = find_offset p2 p1 j i max_dist in
          {value=(score_rec x (y-offset)).value;
           trace=Del}
      else
        max_elt [
          {value=(score_rec (x-1) y).value;
           trace=Ins};
          {value=(score_rec x (y-1)).value;
           trace=Del};
          {value=(score_rec (x-1) (y-1)).value +
                 (max_dist - Position.distance p1.(i) p2.(j));
           trace=Match}
        ] ~compare:(fun t1 t2 -> compare t1.value t2.value)

  let memo_rec ff =
    let h = Hashtbl.create 0 in
    let rec f x y  =
      try Hashtbl.find h (x,y)
      with Not_found ->
        let v = ff f x y in
        Hashtbl.add h (x,y) v;
        v
    in f

  (* let memo_score = memo_rec open_score *)

  let rec align p1 p2 score_fun i j ali  =
    if i = 0 && j = 0 then
      ali
    else
      let score = score_fun i j in
      begin match score.trace with
        |Ins -> let pair = Position.(Pos p1.(i-1) , Gap) in
          align p1 p2  score_fun (i-1) j (pair::ali)
        |Del -> let pair = Position.(Gap, Pos p2.(j-1)) in
          align p1 p2 score_fun i (j-1) (pair::ali)
        |Match -> let pair = Position.(Pos p1.(i-1), Pos p2.(j-1)) in
          align p1 p2 score_fun (i-1)(j-1) (pair::ali)
      end



end

let align_array p1 p2 ~max_dist =
  Array.sort Position.compare p1;
  Array.sort Position.compare p2;
  let memo_score = Align.open_score p1 p2 max_dist |> Align.memo_rec in
  Align.align p1 p2 memo_score (Array.length p1) (Array.length p2) []

let align_list p1 p2 ~max_dist =
  align_array (Array.of_list p1) (Array.of_list p2) ~max_dist:max_dist

let align_bed file1 file2 ~max_dist =
  let p1_tbl = Parser.parse_bed ~file:file1 ~line_parser:Parser.parse_position
               |> Position.partition in
  let p2_tbl = Parser.parse_bed ~file:file2 ~line_parser:Parser.parse_position
               |> Position.partition in
  Hashtbl.create (Hashtbl.length p1_tbl)
  |> Hashtbl.fold (fun chrom p1_list results ->
      match Hashtbl.find_opt p2_tbl chrom with
      |Some p2_list ->
        align_list p1_list p2_list ~max_dist:max_dist
        |> Hashtbl.add results chrom;
        results
      |None -> results
    ) p1_tbl
