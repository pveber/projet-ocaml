open Assignment_bed

val align_array :
  Position.t array  ->
  Position.t array  ->
  max_dist:int ->
  Position.pair list


val align_list :
  Position.t list  ->
  Position.t list  ->
  max_dist:int ->
  Position.pair list

val align_bed :
  string -> string -> max_dist:int ->
  (string, Position.pair list) Hashtbl.t
