let make_instance n min_weight max_weight= 
  Array.map (fun _ ->
  Array.init n (fun _ -> (Random.int (max_weight-min_weight)) + min_weight)
   ) (Array.of_list (Core.List.range 0 n))